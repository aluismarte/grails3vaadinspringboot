package com.alsnightsoft.vaadin.demo.models.queries

import com.alsnightsoft.vaadin.demo.containers.BaseQuery
import com.alsnightsoft.vaadin.demo.containers.BaseQueryFactory
import com.alsnightsoft.vaadin.demo.domains.User
import groovy.transform.CompileStatic

/**
 *  Created by aluis on 11/22/16.
 */
@CompileStatic
class UserQuery extends BaseQuery<User> {

    public UserQuery(BaseQueryFactory baseQueryFactory) {
        super(baseQueryFactory)
    }
}