package grails3vaadinspringboot

import com.alsnightsoft.vaadin.demo.domains.User
import com.alsnightsoft.vaadin.demo.services.UserService
import com.alsnightsoft.vaadin.demo.utils.Grails

class BootStrap {

    def init = { servletContext ->
        createUser("test", "test", "testing", "user")
    }

    private void createUser(String username, String password, String firstnames, String lastnames) {
        User user = Grails.get(UserService).byUsername(username)
        if (!user) {
            user = new User()
            user.username = username
            user.password = password.encodeAsSHA256()
            user.firstnames = firstnames
            user.lastnames = lastnames
            Grails.get(UserService).bootStrap(user)
        }
    }

    def destroy = {
    }
}
