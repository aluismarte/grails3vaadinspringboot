package com.alsnightsoft.vaadin.demo.utils

import groovy.transform.CompileStatic

/**
 *  Created by aluis on 11/22/16.
 */
@CompileStatic
class Themes {

    public static final String DEMO = "Grails3VaadinSpringBoot"
}
