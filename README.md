Example Grails with Vaadin on Spring Boot.

# How to (Configure project)
Configure like Gradle project and later set Grails SDK 3.2.3


# How to configure Vaadin on Grail 3.+

- Add to Application "@ComponentScan(["my.package.one", "my.package.two"])"
- Configure UI using anotation used on DemoUI
- Copy class com.alsnightsoft.vaadin.demo.utils.Grails (Help on service, language and others)
- Check Multi language case "AdmMenu"

## Gradle Configuration (Project case)

- Add on gradle.properties

vaadinVersion=7.7.4

vaadinSpringVersion=1.1.0

vaadinGradleVersion=1.0

- Add Repos to buildscript and conf vaadin plugin.

buildscript {
    
        repositories {
        
            mavenLocal()
            
            maven { url "https://repo.grails.org/grails/core" }
            
            maven { url "https://repo.spring.io/libs-release" }
            
            maven { url "http://maven.vaadin.com/vaadin-addons" }
            
            maven { url "https://plugins.gradle.org/m2/" }
            
        }
        
        dependencies {
        
            classpath "fi.jasoft.plugin:gradle-vaadin-plugin:$vaadinGradleVersion"
            
        }
        
}

- Add module configuration (hibernate conflict)

configurations {

    //noinspection GroovyAssignabilityCheck
    
    'vaadin-client' {
    
        resolutionStrategy.force "javax.validation:validation-api:1.0.0.GA"
        
        exclude module: 'spring-boot-starter-web'
        
        exclude module: 'spring-boot-vaadin'
        
    }
    
}

- Add repositories to default

repositories {

    mavenLocal()
    
    maven { url "https://repo.grails.org/grails/core" }
    
    maven { url "https://repo.spring.io/libs-release" }
    
    maven { url "http://maven.vaadin.com/vaadin-addons" }
    
    maven { url "https://plugins.gradle.org/m2/" }
    
}

- Add vaadin mavenBoom

mavenBom "com.vaadin:vaadin-bom:$vaadinVersion"

- Add vaadin dependencies

compile "com.vaadin:vaadin-spring-boot-starter:$vaadinSpringVersion"

compile "com.vaadin:vaadin-spring-boot:$vaadinSpringVersion"

compile "com.vaadin:vaadin-push:$vaadinVersion"

- Configure vaadin plugin

vaadin {

    //noinspection GroovyAssignabilityCheck
    
    version "$vaadinVersion"
    
    widgetset 'AppWidgetSet'
    
    vaadinTestbench.enabled = false
    
}

# Running (use gradle options add by vaadin gradle plugin to generate stuff and later run normal)

- Firts use vaadinCompile (Generate widgets)
- Second use vaadinCreateTheme (Generate base template if you want it) (optional)
