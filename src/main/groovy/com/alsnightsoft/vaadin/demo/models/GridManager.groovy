package com.alsnightsoft.vaadin.demo.models

import com.alsnightsoft.vaadin.demo.containers.BaseContainer
import com.alsnightsoft.vaadin.demo.containers.BaseQueryFactory
import com.vaadin.ui.Grid
import groovy.transform.CompileStatic

/**
 *  Created by aluis on 11/22/16.
 */
@CompileStatic
class GridManager {

    private BaseContainer container
    private Grid grid

    public GridManager(Grid grid, BaseContainer container) {
        this.grid = grid
        this.container = container
        configureGrid()
    }

    private void configureGrid() {
        grid.setContainerDataSource(container)
        grid.removeColumn(BaseQueryFactory.OBJ)
    }

    public final void clearSelect() {
        grid.deselectAll()
        grid.select(null)
    }

    public final boolean haveSelection() {
        if (isMultiSelect()) {
            return grid.getSelectedRows().size() > 0
        }
        return grid.getSelectedRow()
    }

    public final boolean isMultiSelect() {
        if (grid.getSelectionModel() instanceof Grid.SelectionModel.Multi) {
            return true
        }
        return false
    }

    public final void updateTable() {
        grid.clearSortOrder()
        clearSelect()
    }

    public final Grid getGrid() {
        return grid
    }

    public final void filtered(boolean filter) {
        container.setFiltered(filter)
    }
}
