package com.alsnightsoft.vaadin.demo.ui

import com.alsnightsoft.vaadin.demo.containers.LazyQuery
import com.alsnightsoft.vaadin.demo.domains.User
import com.alsnightsoft.vaadin.demo.models.abstracts.BaseTab
import com.alsnightsoft.vaadin.demo.models.containers.UserContainer
import com.alsnightsoft.vaadin.demo.services.UserService
import com.alsnightsoft.vaadin.demo.utils.Grails
import com.vaadin.event.SelectionEvent
import com.vaadin.ui.Button
import com.vaadin.ui.Notification
import groovy.transform.CompileStatic

/**
 *  Created by aluis on 11/22/16.
 */
@CompileStatic
class UserTab extends BaseTab {

    User userSelect

    public UserTab() {
    }

    @Override
    protected void build() {
        manageGrid(new UserContainer(new LazyQuery<User>() {
            @Override
            int getSize() {
                return Grails.get(UserService).count(showDelete())
            }

            @Override
            List<User> getItemsIds(int startIndex, int numberOfIds) {
                return Grails.get(UserService).list(showDelete(), startIndex, numberOfIds)
            }

            @Override
            int getFilteredSize() {
                return 0
            }

            @Override
            List<User> getFilteredItemsIds(int startIndex, int numberOfIds) {
                return null
            }
        }))
        gridManager.getGrid().addSelectionListener(new SelectionEvent.SelectionListener() {
            @Override
            void select(SelectionEvent event) {
                if (gridManager.haveSelection()) {
                    userSelect = gridManager.grid.getSelectedRow() as User
                } else {
                    userSelect = null
                }
            }
        })
        menu.btnNew.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                Notification.show(Grails.i18n("notification.new"), Grails.i18n("notification.new.description"), Notification.Type.TRAY_NOTIFICATION)
            }
        })
        menu.btnEdit.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                Notification.show(Grails.i18n("notification.edit"), Grails.i18n("notification.edit.description"), Notification.Type.TRAY_NOTIFICATION)
            }
        })
        menu.btnDelete.addClickListener(new Button.ClickListener() {
            @Override
            void buttonClick(Button.ClickEvent event) {
                if (userSelect) {
                    Grails.get(UserService).delete(userSelect)
                    updateTable()
                }
            }
        })
    }
}
