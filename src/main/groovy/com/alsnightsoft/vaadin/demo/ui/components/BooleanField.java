package com.alsnightsoft.vaadin.demo.ui.components;

import com.vaadin.data.util.converter.Converter;
import com.vaadin.ui.*;

/**
 * Demo Custom field
 * <p>
 * Created by aluis on 11/23/16.
 */
public class BooleanField extends CustomField<Boolean> {
    Button button = new Button();

    public BooleanField() {
        setValue(true);
    }

    @Override
    protected Component initContent() {
        button.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                setValue(!(Boolean) getValue());
            }
        });
        return new VerticalLayout(new Label("Click the button"), button);
    }

    @Override
    public Class<Boolean> getType() {
        return Boolean.class;
    }

    @Override
    public void setValue(Boolean newFieldValue) throws com.vaadin.data.Property.ReadOnlyException, Converter.ConversionException {
        button.setCaption(newFieldValue ? "On" : "Off");
        super.setValue(newFieldValue);
    }
}