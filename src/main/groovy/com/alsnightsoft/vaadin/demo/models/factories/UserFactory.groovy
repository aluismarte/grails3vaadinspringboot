package com.alsnightsoft.vaadin.demo.models.factories

import com.alsnightsoft.vaadin.demo.containers.BaseQueryFactory
import com.alsnightsoft.vaadin.demo.containers.LazyQuery
import com.alsnightsoft.vaadin.demo.domains.User
import com.alsnightsoft.vaadin.demo.models.queries.UserQuery
import com.vaadin.data.Item
import groovy.transform.CompileStatic
import org.vaadin.addons.lazyquerycontainer.Query
import org.vaadin.addons.lazyquerycontainer.QueryDefinition

/**
 *  Created by aluis on 11/22/16.
 */
@CompileStatic
class UserFactory extends BaseQueryFactory<User> {

    public UserFactory(LazyQuery<User> lazyQuery) {
        super(lazyQuery)
    }

    @Override
    Item constructItem() {
        return null
    }

    @Override
    Object createProperty(Object propertyID, User dataObject) {
        switch (propertyID.toString()) {
            case OBJ:
                return dataObject
            case "id":
                return dataObject.id
            case "username":
                return dataObject.username
            case "password":
                return dataObject.password
            case "firstnames":
                return dataObject.firstnames
            case "lastnames":
                return dataObject.lastnames
            case "enabled":
                return dataObject.enabled
        }
        return getDefaultProperty(propertyID)
    }

    @Override
    Query constructQuery(QueryDefinition queryDefinition) {
        this.queryDefinition = queryDefinition
        return new UserQuery(this)
    }
}
