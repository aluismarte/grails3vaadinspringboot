package com.alsnightsoft.vaadin.demo.containers;

import org.vaadin.addons.lazyquerycontainer.LazyQueryContainer;
import org.vaadin.addons.lazyquerycontainer.LazyQueryDefinition;
import org.vaadin.addons.lazyquerycontainer.LazyQueryView;

/**
 * Container
 * <p>
 * Created by aluis on 11/22/16.
 */
public abstract class BaseContainer extends LazyQueryContainer {

    protected BaseQueryFactory baseQueryFactory;

    public BaseContainer(BaseQueryFactory baseQueryFactory) {
        super(new LazyQueryDefinition(true, BaseQueryFactory.BATCH_GRID_SIZE, BaseQueryFactory.OBJ), baseQueryFactory);
        this.baseQueryFactory = baseQueryFactory;
        addContainerProperty(BaseQueryFactory.OBJ, Object.class, null, false, false);
        addColumns();
    }

    @SuppressWarnings("unused")
    public void addDebug() {
        addContainerProperty(LazyQueryView.DEBUG_PROPERTY_ID_QUERY_INDEX, Integer.class, 0, true, false);
        addContainerProperty(LazyQueryView.DEBUG_PROPERTY_ID_BATCH_INDEX, Integer.class, 0, true, false);
        addContainerProperty(LazyQueryView.DEBUG_PROPERTY_ID_BATCH_QUERY_TIME, Long.class, 0, true, false);
        addContainerProperty(LazyQueryView.PROPERTY_ID_ITEM_STATUS, Enum.class, 0, true, false);
    }

    public final void addLongColumn(String name) {
        addLongColumn(name, -1L);
    }

    public final void addLongColumn(String name, Long defaultValue) {
        addContainerProperty(name, Long.class, defaultValue, true, false);
    }

    public final void addStringColumn(String name) {
        addStringColumn(name, "");
    }

    public final void addStringColumn(String name, String defaultValue) {
        addContainerProperty(name, String.class, defaultValue, true, false);
    }

    public final void addBooleanColumn(String name) {
        addBooleanColumn(name, true);
    }

    public final void addBooleanColumn(String name, Boolean defaultValue) {
        addContainerProperty(name, Boolean.class, defaultValue, true, false);
    }

    public abstract void addColumns();

    public final int sizeQuery() {
        return baseQueryFactory.sizeQuery();
    }

    public final boolean getFiltered() {
        return baseQueryFactory.getFiltered();
    }

    public final void setFiltered(boolean filtered) {
        baseQueryFactory.setFiltered(filtered);
    }
}