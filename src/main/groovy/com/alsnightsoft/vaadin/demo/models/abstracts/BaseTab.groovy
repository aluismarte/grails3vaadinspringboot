package com.alsnightsoft.vaadin.demo.models.abstracts

import com.alsnightsoft.vaadin.demo.containers.BaseContainer
import com.alsnightsoft.vaadin.demo.models.GridManager
import com.alsnightsoft.vaadin.demo.ui.components.Menu
import com.alsnightsoft.vaadin.demo.utils.Grails
import com.vaadin.data.Property
import com.vaadin.ui.Grid
import com.vaadin.ui.VerticalLayout
import groovy.transform.CompileStatic

/**
 *  Created by aluis on 11/22/16.
 */
@CompileStatic
public abstract class BaseTab extends BasePanel {

    protected VerticalLayout mainLayout = new VerticalLayout()

    protected Menu menu = new Menu()
    private Grid grid = new Grid()
    protected GridManager gridManager

    protected BaseTab() {
        mainLayout.setSizeFull()
        mainLayout.setSpacing(true)

        grid.setSizeFull()

        mainLayout.addComponent(menu)
        mainLayout.addComponent(grid)

        setContent(mainLayout)

        menu.chkShowDelete.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            void valueChange(Property.ValueChangeEvent event) {
                if (menu.chkShowDelete.getValue()) {
                    menu.btnDelete.setCaption(Grails.i18n("action.restore"))
                } else {
                    menu.btnDelete.setCaption(Grails.i18n("action.delete"))
                }
                updateTable()
            }
        })

        build()
    }

    protected void manageGrid(BaseContainer container) {
        gridManager = new GridManager(grid, container)
    }

    public boolean showDelete() {
        return !menu.chkShowDelete.getValue()
    }

    public final void updateTable() {
        if (gridManager) {
            gridManager.updateTable()
        }
    }
}
