package com.alsnightsoft.vaadin.demo.models.containers

import com.alsnightsoft.vaadin.demo.containers.BaseContainer
import com.alsnightsoft.vaadin.demo.containers.LazyQuery
import com.alsnightsoft.vaadin.demo.domains.User
import com.alsnightsoft.vaadin.demo.models.factories.UserFactory
import groovy.transform.CompileStatic

/**
 *  Created by aluis on 11/22/16.
 */
@CompileStatic
class UserContainer extends BaseContainer {

    public UserContainer(LazyQuery<User> lazyQuery) {
        super(new UserFactory(lazyQuery))
    }

    @Override
    public void addColumns() {
        addLongColumn("id")
        addStringColumn("username")
        addStringColumn("firstnames")
        addStringColumn("lastnames")
        addBooleanColumn("enabled")
    }
}
