package com.alsnightsoft.vaadin.demo.models.abstracts

import com.vaadin.ui.Window
import groovy.transform.CompileStatic

/**
 *  Created by aluis on 11/22/16.
 */
@CompileStatic
public abstract class BaseWindow extends Window {

    public BaseWindow() {
        build()
    }

    protected abstract void build()
}
