package com.alsnightsoft.vaadin.demo.containers;

import com.vaadin.data.Item;
import org.vaadin.addons.lazyquerycontainer.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * Do query
 *
 * Created by aluis on 11/22/16.
 */
public abstract class BaseQuery<BEANTYPE> implements Query {

    protected BaseQueryFactory<BEANTYPE> baseQueryFactory;

    protected BaseQuery(BaseQueryFactory<BEANTYPE> baseQueryFactory) {
        this.baseQueryFactory = baseQueryFactory;
    }

    @Override
    public int size() {
        if (baseQueryFactory.getFiltered()) {
            return baseQueryFactory.getLazyQuery().getFilteredSize();
        }
        return baseQueryFactory.getLazyQuery().getSize();
    }

    @SuppressWarnings("Convert2streamapi")
    @Override
    public List<Item> loadItems(int startIndex, int numberOfIds) {
        List<Item> items = new ArrayList<>();
        List<BEANTYPE> allData;
        if (baseQueryFactory.getFiltered()) {
            allData = baseQueryFactory.getLazyQuery().getFilteredItemsIds(startIndex, numberOfIds);
        } else {
            allData = baseQueryFactory.getLazyQuery().getItemsIds(startIndex, numberOfIds);
        }
        for (BEANTYPE data : allData) {
            items.add(baseQueryFactory.constructItem(data));
        }
        return items;
    }

    @Override
    public void saveItems(List<Item> list, List<Item> list1, List<Item> list2) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean deleteAllItems() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Item constructItem() {
        return baseQueryFactory.constructItem();
    }
}